<?php 
declare(strict_types = 1);
require_once "Shape.class.php";

// CHEVRY Clary INFS2_5B

class Rectangle extends Shape 
{
    private $edge1 ; // Float
    private $edge2 ; // Float

    /**
     * Constructeur de la classe Rectangle.
     * Ce constructeur permet d'affecter 2 longueurs de côtés pour les 2 côtés du rectangle.
     * Il fait appel au constructeur de la classe mère en attribuant 4 sommets à la figure.
     * Il affiche aussi les longueurs des deux côtés du rectangle, ainsi que le nombre de sommet de la figure.
     * @param $a1 Longueur d'un côté du rectangle
     * @param $a2 Longueur d'un autre côté (adjacent au précédent) du rectangle.
     */
    public function __construct (float $a1, float $a2)
    {
        parent::__construct(4);
        $this->edge1=$a1;
        $this->edge2=$a2;
        echo "Rectangle ( $a1 , $a2 )\n";
    }

    public function print () : void
    {
        parent::print(); 
        echo "Arete1 : {$this->edge1}\n";
        echo "Arete2 : {$this->edge2}\n";
    }   
}