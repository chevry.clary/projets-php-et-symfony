<?php
declare(strict_types = 1);
require_once "Lodgement.class.php";

// CHEVRY Clary INFS2_5B

function decodeBooleen(bool $b) : String
{
    $res = "Non";
    if ($b === true)
    {
        $res = "Oui";
    }
    return $res;
}


class Apartment extends Lodgement
{
    private $floor; // Int
    private $lift; // Bool

    /**
     * Constructeur de la classe Apartment.
     * Ce constructeur permet d'affecter à un appartement, une surface, un type, un prix du m², l'étage où il se situe ainsi que la présence ou non d'un ascenseur.
     * @param $surface Surface du logement(float)
     * @param $type Type du logement(string)
     * @param $meterPrice Prix du m²(float)
     * @param $floor Etage où se situe l'appartement (int)
     * @param $lift Présence d'un ascenseur (bool)
     */
    public function __construct(float $surface=0, string $type="", float $meterPrice=0, int $floor=0, bool $lift=false)
    {
        parent::__construct($surface,$type,$meterPrice);
        $this->floor = $floor;
        $this->lift = $lift;
    }

    /**
     * Méthode permettant d'afficher une instance de la classe Apartment sous la forme d'un string.
     * Affiche la surface, le type, le prix du m² de la classe mère et l'étage ainsi que la présence d'un ascenseur de l'appartement
     * @return String contenant la surface, le type, le prix du m², l'étage
     * et la présence ou non d'un ascenseur pour l'appartement.
     */
    public function __toString() : string
    {
        $res = parent::__toString();
        $res .= "Etage : {$this->floor}\n";
        $asc = decodeBooleen($this->lift);
        $res .= "Ascenseur : {$asc}\n";
        return $res;
    }

    /**
     * Méthode permettant de connaître le prix d'un appartement.
     * Retourne le prix de l'appartement sous la forme d'un float.
     * @return Prix de l'appartement (float)
     */
    public function getPrice() : float
    {
        $prix = parent::getPrice();
        if ($this->floor == 0)
        {
            $prix = 0.85*$prix;
        }
        elseif ($this->floor >= 2 && $this->floor <= 4 && $this->lift == true)
        {
            for($i = 0 ; $i < $this->floor ; $i++)
            {
                $prix = $prix * 1.05;
            }
        }
        elseif ($this->floor >= 2 && $this->floor <= 4 && $this->lift == false)
        {
            for($i = 0 ; $i < $this->floor ; $i++)
            {
                $prix = $prix * 0.95;
            }
        }
        return $prix;
    }

}