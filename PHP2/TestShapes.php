<?php
declare(strict_types = 1);
require_once "Shape.class.php";
require_once "Rectangle.class.php";
require_once "Square.class.php";

// CHEVRY Clary INFS2_5B

echo "Question 1 \n";
$figure1 = new shape(6);

// $figure1 possède un attribut d'instance nommé vertices et ayant pour valeur 6.
// mode public : accesible de l'extérieur, accessible des classes dérivées, accessibles de l'extérieur.
// mode private : inaccessible de l'extérieur, inaccessible des classes dérivées, accessible de l'intérieur.
// mode protected : inaccessible de l'extérieur, accessible des classes dérivées, accessible des classes dérivées, accessible de l'intérieur.

echo "\nQuestion 2 \n";

// class Rectangle extends Shape

echo "\nQuestion 3 \n";

$rectangle1 = new rectangle(25,63.5);
var_dump($rectangle1);

// edge1 qui est un float et qui a pour valeur 25
// edge2 qui est un float et qui a pour valeur 63.5
// vertices qui est un int et qui a pour valeur 4
// L'instance $rectangle possède l'attribut vertices de sa classe mère shape.
// L'instruction parent::__construct(4) représente l'appel au constructeur de la classe mère avec le paramètre 4.
// L'ordre n'a pas d'importance dans le constructeur

echo "\nQuestion 4 \n";

// L'instruction parent::print() correspond à l'appel de la fonction print de la classe mère.
// Lorsque cette instruction n'est pas présente dans cette méthode, seul l'attribut vertices de la classe mère ne sera pas affiché.
// L'ordre des instructions dans cette méthode n'a pas d'importance.
// On pourrait remplacer l'instruction parent::print() par echo "Sommets : $this->vertices\n";

echo "\nQuestion 5\n";
$carre1 = new Square(14);
// Elle possède des attributs de Shape et Rectangle.
// L'instance $carre 1 est une une instance de Shape de classe et Rectangle à la fois.
// parent::_construct() correspond à un appel du constucteur de rectangle.
// Il est impossible d'appeler le constructeur de la classe Shape.

echo "\nQuestion 6\n";

// Il est impossible de redéfinir la méthode print dans la classe Square car les attributs de la classe rectangle sont en private