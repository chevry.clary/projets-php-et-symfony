<?php
declare(strict_types = 1);

// CHEVRY Clary INFS2_5B

class Compte 
{
    private $numero; // Int
    private $titulaire; // String
    private $solde; // Float

    /**
     * Constructeur de la classe Compte.
     * Ce constructeur permet d'affecter à un compte, un numéro, un titulaire et un solde.
     * Si l'attribut solde n'est pas précisé, il est initialisé à 0.
     * @param $numero Numéro du compte.
     * @param $titulaire Nom du titulaire du compte.
     * @param $solde Solde du compte.
     */
    public function __construct(string $titulaire, int $numero, float $solde=0)
    {
        $this->numero = $numero;
        $this->titulaire = $titulaire;
        $this->solde = $solde;
    }

    /**
     * Accesseur au solde d'un compte.
     * Retourne le solde d'un compte sous la forme d'un float.
     * @return Solde du compte.
     */
    public function getSolde() : float 
    {
        return $this->solde;
    }

    /**
     * Méthode permettant d'afficher une instance de la classe compte sous la forme d'un string.
     * Retourne un string qui représente le compte.
     * @return String qui contient les informations du compte.
     */
    public function __toString() : string
    {
        $res = "Numéro de compte   : {$this->numero}\n";
        $res .= "Titulaire          : {$this->titulaire}\n";
        $res .= "Solde              : {$this->getSolde()} €\n";
        return $res;
    }

    /**
     * Méthode permetttant d'effectuer un retrait sur le compte.
     * Retourne un booléen.
     * Cette méthode affiche également le numéro, le type de l’opération, le montant de l’opération ainsi que le nouveau solde du compte.
     * @param $montant Montant du retrait
     * @return Booléen.
     */
    public function effectuerRetrait(float $montant) : bool
    {
        $old = $this->solde;
        $this->solde -= $montant;
        $res = "Numéro de compte   : {$this->numero}\n";
        $res .= "Titulaire          : {$this->titulaire}\n";
        $res .= "Opération          : Retrait\n";
        $res .= "Montant            : $montant €\n";
        $res .= "Ancien solde       : $old €\n";
        $res .= "Nouveau solde      : $this->solde €\n";
        echo $res;
        return true;
    }

    /**
     * Méthode permetttant d'effectuer un dépôt sur le compte.
     * Retourne un booléen.
     * Cette méthode affiche également le numéro, le type de l’opération et le montant de l’opération ainsi que le nouveau solde du compte.
     * @param $montant Montant du dépôt
     */
    public function effectuerDepot(float $montant) : void
    {
        $old = $this->solde;
        $this->solde += $montant;
        $res = "Numéro de compte   : {$this->numero}\n";
        $res .= "Titulaire          : {$this->titulaire}\n";
        $res .= "Opération          : Dépôt\n";
        $res .= "Montant            : $montant €\n";
        $res .= "Ancien solde       : $old €\n";
        $res .= "Nouveau solde      : $this->solde €\n";
        echo $res;
    }

    /**
     * Méthode permettant de simuler un virement entre deux comptes.
     * Cette méthode prend en paramètres le montant du virement et le compte destinataire.
     * Elle retourne true si le virement est possible, sinon elle retourne false.
     * @param $montant Montant du virement à effectuer.
     * @param $destinataire Compte destinataire du virement.
     * @return Booléen traduisant la possibilité de l'opération de virement.
     */
    public function effectuerVirement(float $montant, Compte $destinataire) : bool
    {
        if ($montant > $this->getSolde())
        {
            $res = false;
            echo "Virement impossible\n";
        }
        else
        {
            echo "Virement possible \n";
            $this->effectuerRetrait($montant);
            echo "\n";
            $destinataire->effectuerDepot($montant);
            echo "\n";
            $res = true;
        }
        return $res;
    }

}