<?php
declare(strict_types = 1);

// CHEVRY Clary INFS2_5B

class Courant extends Compte
{

    private $decouvert; //float

    /**
     * Constructeur de la classe Courant.
     * Ce constructeur fait appel au constructeur de la classe parent afin d'attribuer un numéro, un titulaire et un solde au compte courant.
     * Le constructeur permet d'attribuer un montant maximal de decouvert au compte courant.
     * @param $numero Numéro du compte courant
     * @param $titulaire Titulaire du compte courant
     * @param $solde Solde du compte courant
     * @param $decouvert Montant maximal de découvert autorisé.
     */
    public function __construct(string $titulaire="Inconnu", int $numero=0, float $solde=0, float $decouvert=0)
    {
        parent::__construct($titulaire, $numero, $solde);
        $this->decouvert = $decouvert;
    }

    /**
     * Accesseur au découvert du compte courant.
     * Retourne le montant du découvert.
     * @return Montant maximal du découvert autorisé.
     */
    public function getDecouvert() : float
    {
        return $this->decouvert;
    }

    /**
     * Modificateur du montant du découvert maximal autorisé d'un compte.
     * Affecte une valeur au découvert maximal autorisé.
     * @param $decouvert Montant que l'on veut affecter au découvert maximal autorisé.
     */
    public function setDecouvert(float $decouvert) : void
    {
        $this->decouvert = $decouvert;
    }

    /**
     * Méthode permettant d'effectuer un retrait sur le compte courant.
     * Retourne un booléen.
     * Cette méthode affiche également le numéro, le type de l’opération, le montant de l’opération ainsi que le nouveau solde du compte.
     * @param $montant Montant du retrait
     * @return Booléen
     */
    public function effectuerRetrait(float $montant) : bool
    {
        if ( ($this->getSolde() - $montant) >= 0 )
        {
            $res = true;
            parent::effectuerRetrait($montant);
        }
        else
        {
            $cdc = "Retrait impossible \n";
            $res = false;
            echo $cdc;
        }
        return $res;
    }

    /**
     * Méthode permettant d'afficher toutes les informations d'une instance de la classe Courant sous la forme d'un string.
     * Retourne un string qui représente le compte courant.
     * @return String qui contient les informations du compte courant.
     */
    public function __toString() : string
    {
        $res = parent::__toString();
        $res .= "Découvert autorisé : {$this->getDecouvert()} €\n";
        return $res;
    }
}
