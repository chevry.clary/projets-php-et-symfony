<?php
declare(strict_types = 1);

// CHEVRY Clary INFS2_5B

class Lodgement 
{
    private $surface; // Float
    private $type; // String
    private $meterPrice; // Float

    /**
     * Constructeur de la classe Lodgement.
     * Permet d'affecter une surface, un type, et un prix au mètre carré au logement.
     * @param $surface Surface du logement
     * @param $type Type du logement
     * @param $meterPrice Prix au mètre carré du logement
     */
    public function __construct(float $surface=0, string $type="", float $meterPrice=0)
    {
        $this->surface = $surface;
        $this->type = $type;
        $this->meterPrice = $meterPrice;
    }

    /**
     * Accesseur au prix du m² du logement
     * @return Prix du m² du logement
     */
    public function getMeterPrice() : float
    {
        return $this->meterPrice;
    }

    /**
     * Modificateur du prix du m² du logement.
     * @param $price Prix du m² du logement.
     */
    public function setMeterPrice(float $price) : void
    {
        $this->meterPrice = $price;
    }

    /**
     * Méthode permettant d'afficher une instance de Lodgement sous la forme d'un string.
     * Affiche la surface, le type et le prix du m² du logement.
     * @return String contenant la surface, le type et le prix du m² du logement.
     */
    public function __toString() : string
    {
        $res = "Surface : {$this->surface}\n";
        $res .= "Type : {$this->type}\n";
        $res .= "Prix du m² : {$this->getMeterPrice()}\n";
        return $res;
    }

    /**
     * Méthode permettant de calculer le prix du logement.
     * Retourne le prix du logement sous la forme d'un float.
     * @return Prix du logement.
     */
    public function getPrice() : float
    {
        return $this->surface * $this->getMeterPrice();
    }
}