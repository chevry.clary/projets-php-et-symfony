<?php
declare(strict_types = 1);
require_once "Compte.class.php";
require_once "Courant.class.php";
require_once "Livret.class.php";

// CHEVRY Clary INFS2_5B

echo "Question 1\n";
$compte1 = new Compte ("Lagaffe" , 50408); 
$compte2 = new Compte ("Dupont" , 50409 , 3000.);

echo "Question 3\n";
echo "Solde du compte de M. Dupont : {$compte2->getSolde()} €\n";
echo "Solde du compte de M. Lagaffe : {$compte1->getSolde()} €\n\n";

echo "Question 4\n";
echo "Compte de M. Dupont :\n$compte2\n";
echo "Compte de M. Lagaffe :\n$compte1\n";

echo "Question 5 \n";
echo "Compte de M. Dupont avant retrait :\n$compte2\n\n";
echo "Compte de M. Dupont après retrait de 2500€ :\n";
$compte2->effectuerRetrait(2500);

echo "Compte de M. Lagaffe avant dépôt :\n$compte1\n\n";
echo "Compte de M. Lagaffe après dépôt de 2000€ :\n";
$compte1->effectuerDepot(2000);

echo "\n\nQuestion 7 \n";
$compte3 = new Courant("Robert" , 45750 , 3450);
$compte4 = new Courant("Sacha",  52500);

echo "\nQuestion 8\n";
echo "Découvert autorisé : {$compte3->getDecouvert()} €\n";
$compte3->setDecouvert(1000);
echo "Nouveau découvert autorisé : {$compte3->getDecouvert()} €\n\n";

echo "\nQuestion 9\n";

// Redéfinir la méthode effectuerDepot n'est pas nécessaire car la méthode a exactement le même comportement que celle de la classe mère.

echo "Compte de M. Robert avant retrait :\n$compte3\n";
echo "Compte de M. Robert après retrait de 2500€ :\n";
$compte3->effectuerRetrait(2500);

echo "\nCompte de M. Robert avant retrait :\n$compte4\n";
echo "Compte de M. Robert après retrait de 2500€ (impossible):\n";
$compte4->effectuerRetrait(2500);

echo "\nQuestion 10\n";
echo "Compte de M. Robert :\n$compte3\n";

echo "Question 12\n";
$compte5 = new Livret("Patrick" , 37500 , 6500, 2.25, 1600, 500);
$compte6 = new Livret("Jean",  45000, 5000);

echo "Question 13\n";

// Redéfinir la méthode effectuerDepot n'est pas nécessaire car la méthode a exactement le même comportement que celle de la classe mère.

echo "Compte de M. Patrick avant retrait :\n$compte5\n";
echo "Compte de M. Patrick après retrait de 2500€ :\n";
$compte5->effectuerRetrait(2500);

echo "\nCompte de M. Jean avant retrait :\n$compte6\n";
echo "Compte de M. Jean après retrait de 6000€ :\n";
$compte6->effectuerRetrait(6000);

echo "Question 14 \n";
echo "\nCompte de M. Patrick avant prise d'intérêts :\n$compte5\n";
echo "\nCompte de M. Patrick après prise d'intérêts :\n";
$compte5->priseInterets();

echo "\nQuestion 15\n";
echo "\nCompte de M. Patrick :\n$compte5\n";

echo "Question 16\n";
$compte = new Compte("M. François",7500,2000);
$compteCourant = new Courant("M. Bernard",15000,3000,1000);
$compteLivret = new Livret("M. Xavier",22500,1500,2.25,1600,1200);
echo $compte."\n";
echo $compteCourant."\n";
echo $compteLivret."\n";
echo "Simulation de prise d'intérêt avant les opérations :\n\n";
$compteLivret->priseInterets();
echo "Simulation de dépôts de 1500 €\n\n";
$compte->effectuerDepot(1500);
echo "\n";
$compteCourant->effectuerDepot(1500);
echo "\n";
$compteLivret->effectuerDepot(1500);
echo "\n";
echo "Simulation de retrait de 2000 €\n\n";
$compte->effectuerRetrait(2000);
echo "\n";
$compteCourant->effectuerRetrait(2000);
echo "\n";
$compteLivret->effectuerRetrait(2000);
echo "\n";
echo "Simulation de prise d'intérêt après les opérations :\n\n";
$compteLivret->priseInterets();

echo "\nQuestion 17\n\n";
echo "Compte Courant avant virement de 1000€ sur Compte Livret :\n$compteCourant\n";
echo "Compte Livret avant virement :\n$compteLivret\n";
echo "Après virement :\n";
$compteCourant->effectuerVirement(1000,$compteLivret);

//Il n'est pas nécessaire de redéfinir effectuerVirement car elle utilise les fonctions propres à chaque type de compte grâce au polymorphisme.

//Le polymorphisme permet aux classes ayant des méthodes de même nom d'utiliser automatiquement les méthodes définies dans leur propre classe plutôt qu'une autre.

// Il est possible de définir une méthode abstraite au niveau de la classe Compte donc oui
// Si on ajoute une méthode abstraite dans la classe Compte, la classe Compte deviendra abstraite
// Si la classe Compte est abstraite, on ne peut pas instancier d'objet de type Compte
// Une méthode abstraite doit toujours être redefinie dans une des classes fille de la classe abstraite donc oui