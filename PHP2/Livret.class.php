<?php
declare(strict_types = 1);

// CHEVRY Clary INFS2_5B

class Livret extends Compte
{
    private $taux; // Float
    private $plafond; // Int
    private $soldeMin; // Float

    /**
     * Constructeur de la classe Livret.
     * Ce constructeur fait appel au constructeur de la classe parent afin d'attribuer un numéro, un titulaire et un solde au livret.
     * Le constructeur permet d'attribuer un taux, un plafond et un solde minimum au Livret.
     * @param $numero Numéro du Livret
     * @param $titulaire Titulaire du Livret
     * @param $solde Solde du Livret
     * @param $taux Taux d'intérêts du Livret
     * @param $plafond Plafond du Livret
     * @param $soldeMin Solde minimum du Livret
     */
    public function __construct(string $titulaire="Inconnu", int $numero=0, float $solde=0, float $taux=0, int $plafond=0, float $soldeMin=0)
    {
        parent::__construct($titulaire, $numero, $solde);
        $this->taux = $taux;
        $this->plafond = $plafond;
        $this->soldeMin = $soldeMin;
    }

    /**
     * Méthode permetttant d'effectuer un retrait sur le livret.
     * Retourne un bool.
     * Cette méthode affiche également le numéro, le type de l’opération, le montant de l’opération ainsi que le nouveau solde du compte.
     * @param $montant Montant du retrait
     * @return Booléen
     */
    public function effectuerRetrait(float $montant) : bool
    {
        if ( ($this->getSolde() - $montant) >= 0 )
        {
            $res = true;
            parent::effectuerRetrait($montant);
        }
        else
        {
            $cdc = "Retrait impossible \n";
            $res = false;
            echo $cdc;
        }
        return $res;
    }

    /**
     * Méthode permetttant de simuler la prise d'intérếts dans un livret.
     * Cette méthode calcule les intérêts sur la base du solde minimum.
     */
    public function priseInterets() : void
    {
        if ($this->soldeMin <= $this->plafond)
        {
            $interet = ($this->taux/100) * $this->soldeMin;
        }
        else
        {
            $interet = ($this->taux/100) * $this->plafond;
        }
        
        $this->effectuerDepot($interet);
    }

    /**
     * Méthode permettant d'afficher toutes les informations d'une instance de la classe Livret sous la forme d'un string.
     * Retourne un string qui représente le livret.
     * @return String qui contient les informations du livret.
     */
    public function __toString() : string
    {
        $res = parent::__toString();
        $res .= "Taux d'intérêt     : {$this->taux} %\n";
        $res .= "Plafond            : {$this->plafond} €\n";
        $res .= "Solde minimum      : {$this->soldeMin} €\n";
        return $res;
    }
}