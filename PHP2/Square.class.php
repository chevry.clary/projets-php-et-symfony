<?php
declare(strict_types = 1);
require_once "Rectangle.class.php";

// CHEVRY Clary INFS2_5B

class Square extends Rectangle 
{

    /**
     * Constructeur de la classe Square.
     * Ce constructeur permet d'affecter un float identique aux deux côtés adjacents du carré en faisant appel au constructeur de la classe parent.
     * Affiche également la longueur d'un côté du carré et le nombre de sommets.
     * @param $a1 Longueur du côté du carré.
     */
    public function __construct ( float $a1)
    {
        parent::__construct ($a1,$a1);	
        echo "Square ( $a1 )\n";
    }
}