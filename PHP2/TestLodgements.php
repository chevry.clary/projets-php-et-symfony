<?php
declare(strict_types = 1);
require_once "Lodgement.class.php";
require_once "Apartment.class.php";
require_once "House.class.php";

// CHEVRY Clary INFS2_5B

echo "\nQuestion 2 \n";
$logement1 = new Lodgement(46.7, "T2", 6805.);

echo "\nQuestion 3 \n";
echo "Prix du m² : {$logement1->getMeterPrice()} €\n\n";
$logement1->setMeterPrice(200);
echo "Nouveau prix du m² : {$logement1->getMeterPrice()} €\n\n";

echo "Question 4\n";
echo "$logement1\n";

echo "Question 5\n";

//Les attributs sont de type scalaire il n'est donc pas nécessaire de définir __clone.

$copie = clone $logement1;
echo "Copie : \n$copie\n";
echo "Original : \n$logement1\n";
$copie->setMeterPrice(400);
echo "Copie après sa modification: \n$copie\n";
echo "Original après modification de la copie : \n$logement1\n";

echo "Question 6\n";
echo "Logement : \n$logement1\n";
echo "Prix du logement : {$logement1->getPrice()} €\n\n";

echo "Question 9\n";
$appart = new Apartment (63,"F4",400,5,true);

echo "\nQuestion 10\n";
echo $appart;

echo "Question 11 \n";

//Les attributs sont de type scalaire il n'est donc pas nécessaire de définir __clone.

$copieApart = clone $appart;
echo "Copie : \n$copieApart\n";
echo "Original : \n$appart\n";
$copieApart->setMeterPrice(700);
echo "Copie après sa modification: \n$copieApart\n";
echo "Original après modification de la copie : \n$appart\n";

echo "Question 12 \n\n";
$prixAp = $appart->getPrice();
echo("$prixAp");

echo "Question 14 \n";
$house =  new House(400,"A1",800,200);

echo "\nQuestion 15 \n";
echo "Prix de la maison : {$house->getPrice()} €\n\n";