<?php
declare(strict_types = 1);
require_once "Lodgement.class.php";

// CHEVRY Clary INFS2_5B

class House extends Lodgement
{
    private $gardenArea; // Float

    /**
     * Constructeur de la classe House.
     * Ce constructeur permet d'affecter à une maison, un surface, un type, un prix au m² et une surface de jardin.
     * @param $surface Surface du logement, donc de la maison (float)
     * @param $type Type du logement, donc de la maison (string)
     * @param $meterPrice Prix du m² du logement, donc de la maison (float)
     * @param $gardenArea Surface du jardin de la maison.
     */
    public function __construct(float $surface=0, string $type="", float $meterPrice=0,float $gardenArea=0)
    {
        parent::__construct($surface,$type,$meterPrice);
        $this->gardenArea = $gardenArea;
    }

    /**
     * Méthode permettant de calculer le prix de la maison.
     * Retourne le prix de la maison sous la forme d'un float.
     * @return Prix de la maison (float).
     */
    public function getPrice() : float
    {
        $prixBase = parent::getPrice();
        $prixJardin = $this->gardenArea * 0.1 * $this->getMeterPrice();
        return $prixJardin + $prixBase;
    }
}