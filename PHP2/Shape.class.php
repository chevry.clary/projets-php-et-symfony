<?php
declare(strict_types = 1);

// CHEVRY Clary INFS2_5B

class Shape 
{
    protected $vertices; // Int 

    /**
     * Constructeur de la classe Shape.
     * Permet d'affecter un nombre de sommets aux instances de Shape.
     * Affiche également le nombre de sommets lors de l'instanciation d'un objet de la classe Shape.
     * @param $sommets Nombre de sommet de la figure.
     */
    public function __construct (int $sommets)
    {
        $this->vertices = $sommets ;
        echo "Shape ( $sommets ) \n"; 
    }

    public function print () : void
    {
        echo "Sommets : $this->vertices\n";
    }
}