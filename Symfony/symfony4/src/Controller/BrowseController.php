<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Entity\Album;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BrowseController extends AbstractController
{
    /**
     * @Route("/browse", name="browse")
     */
    public function index(): Response
    {
        return $this->render('browse/index.html.twig', [
            'controller_name' => 'BrowseController',
        ]);
    }

    /**
     * @Route("/artists")
     */
    public function artists()
    {
        $users = $this->getDoctrine()->getRepository(Artist::class)->findBy(array(),array('name' => ('ASC')));
        return $this->render('browse/artists.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/albums/{artistId}", name="albums", requirements={"artistId"="\d+"})
     */
    public function albums(int $artistId)
    {
        $artist = $this->getDoctrine()->getRepository(Artist::class)->findOneBy(['id' => $artistId]);
        if(!$artist)
        {
            throw $this->createNotFoundException('Cet ID ne correspond pas à un artiste');
        }
        return $this->render('browse/albums.html.twig', ['artiste' => $artist]);
    }

    /**
     * @Route("/tracks/{album}", name="tracks")
     * @Entity("album", expr="repository.findWithTracksAndSongs(album)")
     */
    public function tracks(Album $album) : Response
    {
        return $this->render('browse/tracks.html.twig', ['album' => $album]);
    }
}
