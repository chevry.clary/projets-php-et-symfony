<?php

namespace App\Controller;

use App\Entity\Album;
use App\Form\AlbumType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/album", name="album_")
 */
class AlbumController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        return $this->render('album/index.html.twig', [
            'controller_name' => 'AlbumController',
        ]);
    }

    /**
     * @Route("/edit/{album}", name="edit", requirements={"album"="\d+"})
     */
    public function edit(Album $album): Response
    {
        $form = $this->createForm(AlbumType::class, $album);
        return $this->render('album/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/delete/{album}", name="delete", requirements={"album"="\d+"})
     */
    public function delete(Album $album): Response
    {
        return $this->render('album/delete.html.twig', ['album' => $album]);
    }

//    /**
//     * @Route("/new", name="new")
//     */
//    public function new(): Response
//    {
//        $queryBuilder = $this->createQueryBuilder ( 'album' );
//        $res = $queryBuilder->getQuery()->getSingleResult();
//        return $this->render('album/new.html.twig', ['' => ]);
//    }
}
