<?php
declare(strict_types=1);


namespace App\Tests\Controller;

use App\Controller\BrowseController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BrowseControllerTest extends WebTestCase
{

    public function testArtists()
    {
        $client = static::createClient();

        $client->request('GET', '/artists');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertSame("Liste des artistes", $client->getCrawler()->filter('h1')->text());

        $list = $client->getCrawler()->filter('li');

        $this->assertCount(89, $list);

        $this->assertSame("A Perfect Circle", $list->first()->text());

        $this->assertSame("ZZ Top", $list->last()->text());
    }

    public function testAlbums()
    {
        $client = static::createClient();

        $client->request('GET', '/albums/17');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertSame("Liste des albums de Metallica", $client->getCrawler()->filter('h1')->text());

        $list = $client->getCrawler()->filter('li');

        $this->assertCount(18, $list);

        $this->assertSame("Sucking My Love", $list->first()->text());

        $this->assertSame("The Big 4: Live From Sofia, Bulgaria", $list->last()->text());
    }

    public function testArtistsLink()
    {
        $client = static::createClient();

        $client->request('GET', '/artists');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->clickLink('Metallica');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertSame("Liste des albums de Metallica", $client->getCrawler()->filter('h1')->text());

        $list = $client->getCrawler()->filter('li');

        $this->assertCount(18, $list);

        $this->assertSame("Sucking My Love", $list->first()->text());

        $this->assertSame("The Big 4: Live From Sofia, Bulgaria", $list->last()->text());
    }

    public function testAlbumLink()
    {
        $client = static::createClient();

        $client->request('GET', '/artists');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->clickLink('Metallica');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertSame("Liste des albums de Metallica", $client->getCrawler()->filter('h1')->text());

        $list = $client->getCrawler()->filter('li');

        $this->assertCount(18, $list);

        $this->assertSame("Sucking My Love", $list->first()->text());

        $this->assertSame("The Big 4: Live From Sofia, Bulgaria", $list->last()->text());

        $client->clickLink('Sucking My Love');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertSame("Liste des sons de l'album 'Sucking My Love' de 'Metallica'", $client->getCrawler()->filter('h1')->text());

        $list = $client->getCrawler()->filter('li');

        $this->assertCount(11, $list);

        $this->assertSame("Sucking my Love", $list->first()->text());

        $this->assertSame("Hit The Lights", $list->last()->text());
    }
}
