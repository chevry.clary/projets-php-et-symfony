<?php

declare(strict_types = 1);

// DJEMILI Samy & CHEVRY Clary - TP5


class Player 
{

    // Question 17
    /*Attributs*/
    private $nickname; // String
    private $hand; //Pile



    /*Constructeur*/

    /**
     * Constructeur de la classe Player. Ce constructeur permet d'affecter un surnom et
     * une main (une pile de carte) à un joueur.   
     * Lorsque ces valeurs ne sont pas renseignées, l'attribut $nickname est initialisé
     * avec une chaîne de caractères vide, tandis que l'attribut $hand est initialisé 
     * avec une Pile vide.
     *
     * @param $nickname Surnom du joueur.
     * @param $hand Main du joueur (Pile de cartes).
     *
     */
    public function __construct(String $nickname = "",Pile $hand = null)
    {
        $this->nickname = $nickname;
        if ($hand === null)
        {
            $this->hand = new Pile;
        }
        $this->hand = $hand;
    }


    // Question 19
    /*Accesseurs*/

    /**
     * Accesseur au surnom du joueur.
     * Retourne le nom du joueur sous la forme d'une chaîne de caractères.
     *
     * @return Nom du joueur sous la forme d'une chaîne de caractères.
     *
     */
    public function getNickname() : String
    {
        return $this->nickname;
    }


    //Question 20
    /*Autres méthodes*/

    /**
     * Méthode permettant de connaître le nombre de cartes dans la main d'un joueur.
     * Retourne le nombre de carte dans la main d'un joueur sous la forme d'un entier.
     *
     * @return Nombre de cartes dans la main d'un joueur.
     *
     */
    public function getHandCardsCount() : int
    {
        return $this->hand->getCardsNumber();
    }



    // Question 21

    /**
     * Méthode permettant au joueur de joueur la première carte de sa main.
     * Retourne la carte jouée par le joueur.
     * Si la main du joueur ne contient aucune carte, la méthode lance une exception de
     * type OutOfRangeException.
     *
     * @throws Lève l'exception OutOfRangeException si la main du joueur est vide.
     * @return Première carte de la main du joueur (qui a été jouée)
     *
     *
     */
    public function playCard() : Carte // throw OutOfRangeException
    {
        if($this->getHandCardsCount() == 0)
             throw new OutOfRangeException ("playCard - la main du joueur est vide");

        $carte = $this->hand->getCard(0);
        $this->hand->popCard();
        return $carte;
    }



    // Question 22 

    /**
     * Méthode permettant d'ajouter un tas de cartes (passé en paramètre) à la fin de la
     * main d'un joueur.
     * Cette méthode ne retourne rien.
     *
     * @param $tas Tas de cartes à ajouter à la main du joueur.
     *
     */
    public function clearTable(Pile $tas) : void
    {
        $this->hand->addPile($tas);
    }




    // Question 23

    /**
     * Méthode permettant d'affecter un nouveau jeu au joueur à partir d'un tas de cartes
     * passé en paramètres.
     * Cette méthode ne retourne rien.
     *
     * @param $tas Tas de carte qui va constituer le nouveau jeu du joueur.
     *
     */
    public function newGame(Pile $tas) : void
    {
        for ($i = 0 ; $i < $this->hand->getHandCardsCount() ; $i++)
        {
            $this->hand = $tas;
        }
    }



    // Question 24

    /**
     * Méthode permettant d'afficher le surnom d'un joueur ainsi que le nombre de cartes 
     * qu'il a en main.
     * Retourne une chaîne de caractères qui contient le surnom du joueur ainsi que le 
     * nombre de cartes qu'il a en main.
     *
     * @return Chaîne de caractères qui contient le surnom du joueur ainsi que le nombre
     * de cartes qu'il a en main.
     *
     */
    public function __toString() : String
    {
        return "{$this->getNickname()} - {$this->getHandCardsCount()} cartes";
    }
}
