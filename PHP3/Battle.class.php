<?php

declare(strict_types = 1);

// DJEMILI Samy & CHEVRY Clary - TP5


class Battle
{
    //Question 25
    /*Attributs*/

    private $player1; //Player
    private $player2; //Player
    private $playedCards1; //Pile
    private $playedCards2; //Pile



    // Question 26
    /*Constructeur*/

    /**
     * Constructeur de la classe Battle.
     * Ce constructeur permet d'affecter un surnom (chaîne de caractères) et un tas de 
     * cartes (distribuées aléatoirement) à 2 joueurs.
     * Leur tas de cartes provient d'un jeu complet dont le nom (chaîne de caractères) 
     * est précisé dans les paramètres.
     * Si ces paramètres ne sont pas précisés, ils sont tous initialisés avec des chaînes
     * de caractères vides. 
     *
     * @param $player1 Surnom du joueur 1
     * @param $player2 Surnom du joueur 2
     * @param $nomFichier Nom du fichier contenant le jeu de carte propre à la bataille.
     *
     */
    public function __construct(String $player1="", String $player2="", String $nomFichier="")
    {
        if ($nomFichier == "bataille.ini" or $nomFichier == "batailleSimplifie.ini")
        {
            $tas = new Pile($nomFichier);
            $tasJ1 = new Pile;
            $tasJ2 = new Pile;
            
            for ($i = 0 ; $tas->getCardsNumber() !=0 ; $i++)
            {
                $carte = $tas->drawCard();
                if ($i%2 == 0)
                {
                    $tasJ1->addCard($carte);
                }
                else
                {
                    $tasJ2->addCard($carte);
                }
            }

            $this->player1 = new Player($player1,$tasJ1);
            $this->player2 = new Player($player2,$tasJ2);
            $this->playedCards1 = new Pile;
            $this->playedCards2 = new Pile;
         }
    }


    /*Autres méthodes*/

    // Question 27

    /**
     * Méthode permettant de simuler le déroulement d'un tour simple.
     * Chaque joueur joue à son tour une carte sur la table si c'est possible.
     * Cette méthode retourne un booléen permettant de savoir si les joueurs peuvent
     * jouer : true s'ils ont encore des cartes à jouer, false sinon.
     * Cette méthode affichera les 2 cartes posées en utilisant la fonction 
     * printSideBySide.
     * Si un des joueurs ne possède aucune carte, la méthode lance une exception de type 
     * OutOfRangeException.
     *
     * @throws Lève l'exception OutOfRangeException si un des joueurs ne possède plus de 
     * carte à jouer.
     * @return Booléen permettant de savoir si chaque joueur peut jouer une carte, true
     * si cela est possible, false sinon.
     *
     */
    public function singleRound() : bool // throw OutOfRangeException
    {
        $res = true;
        if ($this->player1->getHandCardsCount() == 0)
        {
            $res = false;
            throw new OutOfRangeException ("singleRound - la main du joueur {$this->$player1} est vide");
        }
        if ($this->player2->getHandCardsCount() == 0)
        {
            $res = false;
            throw new OutOfRangeException ("singleRound - la main du joueur {$this->$player2} est vide");
        }

        $carteJ1 = $this->player1->playCard();
        $carteJ2 = $this->player2->playCard();

        $this->playedCards1->addCard($carteJ1);
        $this->playedCards2->addCard($carteJ2);


        printSideBySide($carteJ1,$carteJ2);
        return $res;
    }


    // Question 28
    /**
     * Méthode permettant de simuler le déroulement d'une bataille suivant un tour simple
     * Chaque joueur pose une carte face cachée (s'ils le peuvent), puis une autre carte
     * face visible.
     * Cette méthode retourne un booléen permettant de savoir si les cartes ont pu être
     * jouées : true si cela a été possible, false sinon.
     * Si un des joueurs ne possède aucune carte, la méthode lance une exception de type 
     * OutOfRangeException.
     * 
     * @throws Lève l'exception OutOfRangeException si un des joueurs ne possède plus de 
     * carte à jouer.
     * @return Booléen permettant de savoir si chaque joueur peut jouer une carte, true
     * si cela est possible, false sinon.
     *
     */
    public function battleRound() : bool // throw OutOfRangeException
    {
        $res = true;
        if ($this->player1->getHandCardsCount() == 0)
        {
            $res = false;
            throw new OutOfRangeException ("singleRound - la main du joueur {$this->$player1} est vide");
        }
        if ($this->player2->getHandCardsCount() == 0)
        {
            $res = false;
            throw new OutOfRangeException ("singleRound - la main du joueur {$this->$player2} est vide");
        }


        printSideBySideFaceDown();
        $faceCachee1 = $this->player1->playCard();
        $faceCachee2 = $this->player2->playCard();

        $this->playedCards1->addCard($faceCachee1);
        $this->playedCards2->addCard($faceCachee2);

        $this->singleRound();
    
        return $res;
    }


    // Question 29
    /**
     * Méthode permettant de savoir si le tour suivant sera une bataille en comparant
     * les 2 dernières cartes jouées par les 2 joueurs.
     * Retourne un booléen permettant de savoir si le prochain tour est une bataille :
     * true si le prochain tour est une bataille, false sinon.
     *
     * @return Booléen permettant de savoir si le prochain tour est une bataille : true
     * si c'est une bataille, false sinon.
     *
     */
    public function decideBattle() : bool
    {
        
        $res = false;
        if ($this->playedCards1->showLast()->isEqual($this->playedCards2->showLast()))
        {
            $res = true;
            echo "Bataille \n";
        }

        return $res;
    }



    // Question 30
    /**
     * Méthode permettant d'attribuer le pli au joueur ayant remporté un tour.
     * Cette méthode ne retourne rien, et elle réinitialise les jeux des 2 joueurs une
     * fois la prise effectuée.
     *
     */
    public function clearTable() : void
    {
        if ($this->playedCards1->showLast()->isStronger($this->playedCards2->showLast()))
        {
            $this->player1->clearTable($this->playedCards1);
            $this->player1->clearTable($this->playedCards2);
            echo "\n---------------> Prise : {$this->player1->getNickname()}\n\n";
        }

        if ($this->playedCards2->showLast()->isStronger($this->playedCards1->showLast()))
        {
            $this->player2->clearTable($this->playedCards1);
            $this->player2->clearTable($this->playedCards2);
            echo "\n---------------> Prise : {$this->player2->getNickname()}\n\n";
        }

        $this->playedCards1 = new Pile;
        $this->playedCards2 = new Pile;
    }



    // Question 31
    /**
     * Méthode permettant de déterminer quel joueur est le gagnant à la fin de la partie.
     * Retourne le nom du joueur gagnant.
     *
     * @return Nom du joueur ayant gagné la partie.
     *
     */
    public function determineWinner() : String
    {
        $gagnant = "";
        if($this->player1->getHandCardsCount() == 0)
        {
            $gagnant = $this->player2->getNickname();
        }

        if($this->player2->getHandCardsCount() == 0)
        {
            $gagnant = $this->player1->getNickname();
        }
        return $gagnant;
    }


    // Question 32


    /**
     * Méthode permettant d'afficher l'état des joueurs (surnoms et nombre de cartes en
     * main).
     * Cette méthode retourne une chaîne de caractères contenant l'état des joueurs 
     * (surnoms et nombre de cartes en main).
     *
     * @return Chaîne de chaîne de caractères contenant l'état des joueurs 
     * (surnoms et nombre de cartes en main).
     *
     */
    public function __toString() : String
    {
        $res = "Joueur 1 :\n";
        $res .= "           {$this->player1->getNickname()}                 Cartes en main : {$this->player1->getHandCardsCount()}\n";
        $res .= "Joueur 2 :\n";
        $res .= "           {$this->player2->getNickname()}                 Cartes en main : {$this->player2->getHandCardsCount()}\n\n";

        return $res;
    }

}


    /**
     * Méthode permettant de transformer un booléen passé en paramètre en chaîne de 
     * caractères.
     * Retourne une chaîne de caractères.
     *
     * @param $bool Booléen que l'on veut traduire en chaîne de caractères
     *
     */
    function decodeBooleen(bool $b) : string
    {
        $chaine = "";
        if ($b === true)
            $chaine = "Vrai";
        else
            $chaine = "Faux";
        return $chaine;
    }

    

    /**
    *
    * Fonction prenant en paramètres 2 cartes et qui ne retourne rien.
    * Cette fonction permet d'afficher 2 cartes côte à côte.
    *
    * @param $C1 Première carte à afficher.
    * @param $C2 Deuxième carte à afficher.
    *
    */
    function printSideBySide( Carte  $C1,  Carte  $C2) : void
    {
        printf( "%9s%11s%9s\n","--------"," ", "--------");
        printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
        printf("%1s%6s%3s%10s",'|', $C1->getValue(),"|"," ");
        printf("%1s%6s%3s\n",'|', $C2->getValue() , "|");
        printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
        printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
        printf("%1s%8s%1s%10s",'|',$C1->getColor(),"|"," ");
        printf("%1s%8s%1s\n",'|', $C2->getColor(),"|");
        printf( "%9s%11s%9s\n","--------"," ", "--------");
    }


    /**
     * Fonction permettant d'afficher 2 cartes côte à côte mais face cachée.
     * Ne retourne rien.
     *
     */
    function printSideBySideFaceDown() : void
    {   
        printf( "%9s%11s%9s\n","--------"," ", "--------");
        printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
        printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
        printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
        printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
        printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
        printf( "%9s%11s%9s\n","--------"," ", "--------");
    }
