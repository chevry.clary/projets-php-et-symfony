<?php

declare(strict_types = 1);

// DJEMILI Samy & CHEVRY Clary - TP5 - Exercice 7

require_once "Carte.class.php";
require_once "Pile.class.php";
require_once "Player.class.php";
require_once "Battle.class.php";


$jeu = new Battle("Faker","Caps","bataille.ini");
$compt = 0;


while ($jeu->determineWinner() == "")
{
    system("clear");
    
    echo $jeu;

    $jeu->singleRound();
    
    if ($jeu->decideBattle() == true)
    {
        $jeu->battleRound();
        $jeu->clearTable();
    }
    else
    {
        $jeu->clearTable();
    }
    $jeu->determineWinner();
    $compt += 1;
    echo "Nombre de tours : {$compt}\n\n";
}
