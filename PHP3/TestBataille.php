<?php

declare(strict_types = 1);

// DJEMILI Samy & CHEVRY Clary - TP5


require_once "Carte.class.php";
require_once "Pile.class.php";
require_once "Player.class.php";
require_once "Battle.class.php";



// Question 26

$bataille = new Battle("Faker","Caps","bataille.ini");


// Question 27
echo "Question 27 \n";

$bataille->singleRound();



// Question 28
echo "\nQuestion 28\n";

$bataille->battleRound();




// Question 29
echo "\nQuestion 29\n";
$bataille->singleRound();
echo "Bataille ? : ".decodeBooleen($bataille->decideBattle())."\n\n";




//Question 30
echo "Question 30 \n";
$bataille->singleRound();
$bataille->clearTable();



// Question 32
echo "\nQuestion 32\n";

echo "Etat des joueurs avant le premier tour de jeu : \n\n";
$bataille = new Battle("Faker","Caps","bataille.ini");

echo $bataille;
$bataille->singleRound();
$bataille->clearTable();
echo $bataille;
