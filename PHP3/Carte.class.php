<?php

declare(strict_types = 1);

// DJEMILI Samy & CHEVRY Clary - TP5

class Carte
{
    // Question 1
    /*Attributs*/
    private $value; //String
    private $color; //String
    private $order; //int



    // Question 2

    /**
     * Constructeur de la classe Card. Ce constructeur permet d'affecter une valeur, une
     * couleur et un ordre à une carte.
     * Lorsque ces valeurs ne sont pas renseignées, les attributs valeur et color 
     * seront initialisés par une chaîne de caractères vide, tandis que l'attribut order
     * sera initialisé à 0
     *
     * @param $value Valeur de la carte
     * @param $color Couleur de la carte
     * @param $order Ordre de la carte
     *
     */
    /*Constructeur*/
    public function __construct(String $value="", String $color="", int $order=0)
    {
        $this->value = $value;
        $this->color = $color;
        $this->order = $order;
    }



    // Question 3
    /*Accesseurs*/

    /**
     * Accesseur à la valeur de la carte.
     * Retourne la valeur de la carte sous la forme d'une chaîne de caractères.
     *
     * @return Valeur de la carte sous la forme d'une chaîne de caractères.
     *
     */
    public function getValue() : String
    {
        return $this->value;
    }


    /**
     * Accesseur à la couleur de la carte.
     * Retourne la couleur de la carte sous la forme d'une chaîne de caractères.
     *
     * @return Couleur de la carte sous la forme d'une chaîne de caractères.
     *
     */
    public function getColor() : String
    {
        return $this->color;
    }


    /**
     * Accesseur à l'ordre de la carte.
     * Retourne l'ordre de la carte sous la forme d'un entier.
     *
     * @return Ordre de la carte sous la forme d'un entier.
     *
     */
    public function getOrder() : int
    {
        return $this->order;
    }


    // Question 4
    /*Autres méthodes*/

    /**
     * Méthode permettant d'afficher une instance de la classe Card sous la forme d'une
     * chaîne de caractères.
     * Retourne la chaîne de caractères représentant le contenu de l'instance de la
     * classe Card.
     * 
     * @return Chaîne de caractères représentant le contenu de l'instance de la
     * classe Card.
     *
     *
     */
    public function __toString() : string
    {
        $res = sprintf("%9s","--------")."\n" ;
        $res = $res ."|".sprintf("%9s","|")."\n" ;
        $res = $res .'|'.sprintf("%6s",$this->value) .sprintf("%3s","|")."\n";
        $res = $res ."|".sprintf("%9s","|")."\n" ;
        $res = $res ."|".sprintf("%9s","|")."\n" ;
        $res = $res ."|".sprintf("%8s",$this->color)."|\n" ;
        $res = $res .sprintf("%9s","--------") ;
        return $res;
    }

    /**
     * Méthode permettant de savoir si la carte, sur laquelle on applique cette méthode,
     * est inférieure à la carte passée en paramètre.
     * Retourne un booléen traduisant la véracité de ce propos : true si la carte passée
     * en paramètre est plus grande que la carte sur laquelle on applique cette méthode,
     * false sinon.
     *
     * @param $carte Carte dont on veut savoir si elle est plus grande que la carte sur
     * laquelle on applique la méthode
     * 
     * @return Booléen : true si la carte passée en paramètre est plus grande que la 
     * carte sur laquelle on applique cette méthode, false sinon.
     *
     */
    public function isWeaker(Carte $carte) : bool
    {
        return $this->order < $carte->getOrder();
    }



    /**
     * Méthode permettant de savoir si la carte, sur laquelle on applique cette méthode,
     * est supérieure à la carte passée en paramètre.
     * Retourne un booléen traduisant la véracité de ce propos : true si la carte passée
     * en paramètre est plus petite que la carte sur laquelle on applique cette méthode,
     * false sinon.
     *
     * @param $carte Carte dont on veut savoir si elle est plus petite que la carte sur
     * laquelle on applique la méthode
     * 
     * @return Booléen : true si la carte passée en paramètre est plus petite que la 
     * carte sur laquelle on applique cette méthode, false sinon.
     *
     */
    public function isStronger(Carte $carte) : bool
    {
        return $this->order > $carte->getOrder();
    }



    /**
     * Méthode permettant de savoir si la carte, sur laquelle on applique cette méthode,
     * est de même ordre que la carte passée en paramètre.
     * Retourne un booléen traduisant la véracité de ce propos : true si la carte passée
     * en paramètre est de même ordre que la carte sur laquelle on applique cette 
     * méthode, false sinon.
     *
     * @param $carte Carte dont on veut savoir si elle est de même ordre que la carte sur
     * laquelle on applique la méthode.
     * 
     * @return Booléen : true si la carte passée en paramètre est de même ordre que la 
     * carte sur laquelle on applique cette méthode, false sinon.
     *
     */
    public function isEqual(Carte $carte) : bool
    {
        return $this->order == $carte->getOrder();
    }
}
