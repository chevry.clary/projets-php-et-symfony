<?php

declare(strict_types = 1);

// DJEMILI Samy & CHEVRY Clary - TP5

class Pile
{
    // Question 7
    /*Attributs*/
    private $pack; //array


    // Question 8
    /*Constructeur*/

    /**
     * Constructeur permettant de charger un jeu de cartes à partir d’un fichier dont le
     * nom est passé en paramètre. Si le nom n’est pas précisé à l’appel le tas 
     * initialisé sera vide. Dans le cas contraire, les cartes décrites dans le fichier 
     * sont stockées dans le tas.
     *
     * @param $nomFichier Nom du fichier à partir duquel on veut charger un jeu de cartes
     *
     */
    public function __construct (string $nomFichier="")
        {
            $this->pack = array();
            if($nomFichier != "")
            {
                $fichier = parse_ini_file($nomFichier, true, INI_SCANNER_TYPED) ;
            
                if ($fichier === false)
                    die('Erreur de lecture') ;
                else
                    foreach ($fichier as $couleur => $cartes)
                        foreach ($cartes as $valeur => $ordre)
                            $this->pack[] = new Carte((string)$valeur, (string)$couleur, (int)$ordre);
            }
        }

    // Question 9
    /*Autres méthodes*/

    /**
     * Méthode permettant de connaître le nombre de cartes dont est constitué un paquet.
     * Retourne ce nombre de carte sous la forme d'un nombre entier.
     *
     * @return Retourne le nombre de carte dont est constitué un paquet (sous la forme 
     * d'un entier).
     *
     */
    public function getCardsNumber() : int
    {
        return count($this->pack);
    }



    // Question 10

    /**
     * Méthode permettant d'accéder à la carte correspondant à l'indice passé en 
     * paramètre.
     * Retourne la carte correspondant à l'indice passé en paramètre.
     * Si l'indice $index passé en argument se situe en dehors du tableau, la méthode
     * lance une exception de type OutOfRangeException.
     *
     * @param $i Indice de la carte dans le paquet.
     * @throws Lève l'exception OutOfRangeException si l'indice $index est situé en
     * dehors du tableau.
     *
     */
    public function getCard(int $i) : Carte // throw OutOfRangeException
    {
        if($i < 0 || $i >= count($this->pack))
            throw new OutOfRangeException ( "getCard - indice invalide : $i");
        return $this->pack[$i];
    }



    // Question 11

    /**
     * Méthode ne retournant rien et  permettant d'ajouter une carte passée en paramètre
     * à la fin du paquet.
     *
     * @param $carte Carte que l'on veut ajouter à la fin du paquet.
     *
     */
    public function addCard(Carte $carte) : void
    {
        $this->pack[] = $carte;
    }



    // Question 12

    /**
     * Méthode permettant de retirer la première carte du paquet.
     * Retourne la carte enlevée si le paquet contient des cartes.
     * Si le paquet ne contient aucune carte, la méthode lance une exception de type 
     * OutOfRangeException.
     *
     * @throws Lève l'exception OutOfRangeException si le paquet est vide.
     * @return Retourne la carte enlevée du paquet.
     *
     *
     */
    public function popCard() : Carte // throw OutOfRangeException
    {
        if(count($this->pack) == 0)
            throw new OutOfRangeException ( "popCard - le paquet de cartes est vide");
        
        $inverse = array_reverse($this->pack);
        $carte = array_pop($inverse);
        $this->pack = array_reverse($inverse);
        return $carte;
    }



    // Question 13
    /**
     * Méthode permettant de consulter la dernière carte du paquet.
     * Retourne la dernière carte du paquet si celui-ci n'est pas vide.
     * Si le paquet ne contient aucune carte, la méthode lance une exception de type 
     * OutOfRangeException.
     *
     * @throws Lève l'exception OutOfRangeException si le paquet est vide.
     * @return Retourne la dernière carte du paquet.
     *
     */
    public function showLast() : Carte // throw OutOfRangeException
    {
        if(count($this->pack) == 0)
            throw new OutOfRangeException ( "showLast - le paquet de cartes est vide");
        return $this->pack[count($this->pack)-1];
    }


    // Question 14

    /**
     * Méthode permettant de retirer aléatoirement une carte du tas.
     * Retourne la carte retirée si le paquet n'est pas vide.
     * Si le paquet ne contient aucune carte, la méthode lance une exception de type 
     * OutOfRangeException.
     *
     * @throws Lève l'exception OutOfRangeException si le paquet de cartes est vide.
     * @return Retourne une carte tirée aléatoirement.
     *
     */
    public function drawCard() : Carte // throw OutOfRangeException
    {
         if(count($this->pack) == 0)
             throw new OutOfRangeException ( "drawCard - le paquet de cartes est vide");

         $hasard = rand(0,count($this->pack)-1);
         $carte = $this->pack[$hasard];
         unset($this->pack[$hasard]);
         $this->pack = array_values($this->pack);
         return $carte;
    }



    // Question 15

    /**
     * Méthode permettant d'ajouter un tas de cartes (passé en paramètre) à un autre tas
     * de carte auquel on applique cette méthode.
     * Cette méthode ne retourne rien.
     *
     * @param $tas Tas de carte que l'on désire ajouter à la fin du tas de cartes sur
     * lequel on applique cette méthode.
     *
     */
    public function addPile(Pile $tas) : void
    {
        for ($i = 0 ; $i < count($tas->pack) ; $i++)
        {
            $this->pack[] = $tas->pack[$i];
        }
    }




    // Question 16

    /**
     * Méthode permettant d'afficher toutes les cartes d'une pile sous la forme d'une
     * chaîne de caractères.
     * Retourne la chaîne de caractères représentant le contenu de la pile.
     *
     * @return Chaîne de caractères représentant le contenu de la pile.
     *
     */
    public function __toString() : String
    {
        $res = "";
        for ($i = 0 ; $i < count($this->pack) ; $i++)
        {
            $res .= $this->pack[$i]."\n";
        }
        return $res;
    }
}
