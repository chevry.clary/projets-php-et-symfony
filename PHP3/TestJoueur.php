<?php

declare(strict_types = 1);

// DJEMILI Samy & CHEVRY Clary - TP5

require_once "Player.class.php";
require_once "Pile.class.php";
require_once "Carte.class.php";



// Question 18

$p1 = new Player;
$p2 = new Player("Faker");




// Question 19
echo "Question 19\n";

echo "Nom du joueur \$p2 : {$p2->getNickname()} \n\n";




// Question 20
echo "Question 20 \n";

$asDeCarreaux = new Carte ("As" , "Carreaux" , 13);

$main = new Pile;
$main->addCard($asDeCarreaux);

$j2 = new Player("Joueur 2",$main);

echo "Nombre de cartes dans la main de \$j2 : {$j2->getHandCardsCount()} carte(s) \n\n";



// Question 21
echo "Question 21 \n";

try 
{
    echo "Nombre de cartes dans la main de \$j2 : {$j2->getHandCardsCount()} carte(s) \n\n";
    echo "Carte jouée : \n {$j2->playCard()}\n";
    echo "Nombre de cartes dans la main de \$j2 : {$j2->getHandCardsCount()} carte(s) \n";
    echo "Carte jouée : \n {$j2->playCard()}\n";

}
catch (OutOfRangeException $e)
{
    echo $e->getMessage()."\n";
}



// Question 22
echo "\nQuestion 22 \n";


$jeuCartes2 = new Pile ("bataille.ini");

echo "Nombre de cartes dans \$jeuCartes2 : {$jeuCartes2->getCardsNumber()} cartes \n";
echo "Nombre de cartes dans la main de \$j2 : {$j2->getHandCardsCount()} carte(s) \n\n";

$j2->clearTable($jeuCartes2);

echo "Nombre de cartes dans la main de \$j2 après avoir remporté le pli \$jeuCartes2 : {$j2->getHandCardsCount()} carte(s) \n\n";


// Question 23
echo "Question 23\n";

$c = new Carte ("As", "Carreaux", 13);
$jeu = new Pile;

$jeu->addCard($c);

echo "Le jeu actuel du joueur \$j2 contient {$j2->getHandCardsCount()} cartes \n";
echo "Son nouveau jeu est :\n $jeu \n";
echo "Le jeu actuel du joueur \$j2 contient {$j2->getHandCardsCount()} cartes \n\n";



// Question 24
echo "Question 24 \n";


echo "Joueur 2 : \n $j2 \n";
