<?php

declare(strict_types = 1);
require_once "Carte.class.php";

// DJEMILI Samy & CHEVRY Clary - TP5

// Question 2

$vide = new Carte;
$asDeCoeur = new Carte("As" , "Coeur" , 13);


// Question 3
echo "Question 3 : tests des accesseurs sur l'objet \$asDeCoeur \n";

echo $asDeCoeur->getValue()."\n";
echo $asDeCoeur->getColor()."\n";
echo $asDeCoeur->getOrder()."\n\n";


// Question 4
echo "Question 4 \n";


/**
*
* Fonction prenant en paramètres 2 cartes et qui ne retourne rien.
* Cette fonction permet d'afficher 2 cartes côte à côte.
*
* @param $C1 Première carte à afficher.
* @param $C2 Deuxième carte à afficher.
*
*/
function printSideBySide( Carte  $C1,  Carte  $C2) : void
{
    printf( "%9s%11s%9s\n","--------"," ", "--------");
    printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
    printf("%1s%6s%3s%10s",'|', $C1->getValue(),"|"," ");
    printf("%1s%6s%3s\n",'|', $C2->getValue() , "|");
    printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
    printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
    printf("%1s%8s%1s%10s",'|',$C1->getColor(),"|"," ");
    printf("%1s%8s%1s\n",'|', $C2->getColor(),"|");
    printf( "%9s%11s%9s\n","--------"," ", "--------");
}

/**
 * Fonction permettant d'afficher 2 cartes côte à côte mais face cachée.
 * Ne retourne rien.
*
*/
function printSideBySideFaceDown() : void
{   
    printf( "%9s%11s%9s\n","--------"," ", "--------");
    printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
    printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
    printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
    printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
    printf("%1s%9s%10s%1s%9s\n",'|', "|"," ", '|', "|");
    printf( "%9s%11s%9s\n","--------"," ", "--------");
}



$roiDeCoeur = new Carte("Roi" , "Coeur" , 12);

echo "Affichage de la carte as de coeur sous la forme d'une chaîne de caractères : \n";
echo $asDeCoeur."\n\n";

echo "Affichage de l'as de coeur et du roi de coeur côte à côte :\n";
printSideBySide($asDeCoeur,$roiDeCoeur);

echo "\nAffichage de 2 cartes côte à côte face cachée :\n";
printSideBySideFaceDown();




// Question 5
echo "\nQuestion 5 \n";

function decodeBooleen(bool $b) : string
{
    $chaine = "";
    if ($b === true)
        $chaine = "Vrai";
    else
        $chaine = "Faux";
    return $chaine;
}



echo "L'as de coeur est-il inférieur au roi de coeur ? : ".decodeBooleen($asDeCoeur->isWeaker($roiDeCoeur))."\n\n";

echo "L'as de coeur est-il supérieur au roi de coeur ? : ".decodeBooleen($asDeCoeur->isStronger($roiDeCoeur))."\n\n";

echo "L'as de coeur est-il de même ordre que le roi de coeur ? : ".decodeBooleen($asDeCoeur->isEqual($roiDeCoeur))."\n\n";



// Question 6
// Il n'est pas possible de modifier, depuis l'extérieur de la classe, un 
// objet de type Card : on a mis en place l'encapsulation des données, et de plus, aucun 
// modificateur n'est encore défini.

// Les instances de cette classe ne sont donc pas modifiable après leur instanciation.
