<?php

declare(strict_types = 1);

require_once "Pile.class.php";
require_once "Carte.class.php";

// DJEMILI Samy & CHEVRY Clary - TP5


// Question 8

$jeuCartes = new Pile ("bataille.ini");
$jeuCartesVide = new Pile;



// Question 9
echo "Question 9 \n";

echo "Nombre de cartes dans le paquet \$jeuCartes : {$jeuCartes->getCardsNumber()}\n\n";
echo "Nombre de cartes dans le paquet \$jeuCartes : {$jeuCartesVide->getCardsNumber()}\n\n";




// Question 10
echo "Question 10 \n";

try 
{
    
    echo "Carte d'indice 0 : \n {$jeuCartes->getCard(0)} \n\n";
    echo "Carte d'indice 8000 :\n";
    echo $jeuCartes->getCard(8000);
}

catch (OutOfRangeException $e)
{
    // Traitement de l'erreur
    echo $e->getMessage()."\n";
}




// Question 11
echo "\nQuestion 11 \n";

echo "Nombre de cartes dans le paquet \$jeuCartesVide : {$jeuCartesVide->getCardsNumber()} \n";

echo "Ajout de la carte as de carreaux au paquet \$jeuCartesVide \n";

$asDeCarreaux = new Carte ("As" , "Carreaux" , 13);


$jeuCartesVide->addCard($asDeCarreaux);

echo "Nombre de cartes dans le paquet \$jeuCartesVide : {$jeuCartesVide->getCardsNumber()} \n\n";




// Question 12
echo "Question 12 \n";

try
{
    echo "Nombre de cartes dans le paquet \$jeuCartesVide : {$jeuCartes->getCardsNumber()} \n\n";
    echo "Carte supprimée du paquet : \n{$jeuCartes->popCard()} \n";
    echo "Nombre de cartes dans le paquet \$jeuCartesVide : {$jeuCartes->getCardsNumber()} \n\n";
}
catch (OutOfRangeException $e)
{
    echo $e->getMessage()."\n\n";
}




// Question 13
echo "Question 13 \n";

$paquetVide = new Pile;


try
{
    echo "Dernière carte du paquet :\n {$jeuCartes->showLast()} \n";
    echo "Dernière carte d'un paquet vide :\n";
    echo $paquetVide->showLast()."\n";
}
catch (OutOfRangeException $e)
{
    echo $e->getMessage()."\n";
}





// Question 14
echo "\nQuestion 14 \n";



echo "Taille du paquet de cartes : {$jeuCartes->getCardsNumber()} cartes \n";
echo $jeuCartes->drawCard()."\n";
echo "Taille du paquet de cartes après pioche: {$jeuCartes->getCardsNumber()} cartes \n";




// Question 15
echo "\nQuestion 15 \n";
$asDeCarreaux = new Carte ("As" , "Carreaux" , 13);
$asDeCoeur = new Carte ("As" , "Coeur" , 13);

$jeuCartePile = new Pile;
$jeuCartePile->addCard($asDeCarreaux);
$jeuCartePile->addCard($asDeCoeur);


echo "Taille du paquet de cartes : {$jeuCartes->getCardsNumber()} cartes \n";
$jeuCartes->addPile($jeuCartePile);
echo "Taille du paquet de cartes après ajout d'une pile de 2 cartes : {$jeuCartes->getCardsNumber()} cartes \n";





// Question 16
echo "\n Question 16 : affichage du contenu de la pile \$jeuCartes\n";
echo $jeuCartes."\n"; 
