var ajax = null;
// Fonction appelée au chargement complet de la page
window.onload = function () {
    // Désactivation de l'envoi du formulaire
    document.forms['f'].onsubmit = function () { return false ; }
    // Fonction appelée lors d'une modification de la saisie
    document.forms['f'].elements['art'].onkeyup = function() {
      console.log(document.forms['f'].elements['art'].value);

      if (ajax != null)
        ajax.cancel();

      ajax = new AjaxRequest(
        {
         url : "liste_artistes.php",
         method : "get",
         handleAs : "text",
         parameters : { q : document.forms['f'].elements['art'].value,
                        wait : null},
         onSuccess : function (result) {
          let span = document.getElementById("suggestions");
          span.innerHTML = "Suggestions: " + (document.forms['f'].elements['art'].value) + " =>" + result;
          }
         }
      ) ;
    }
}
