<?php
declare(strict_types=1);

spl_autoload_register(function ($class)
{
  if (file_exists('src'))
  {
    include 'src/' . $class . '.php';
  }
});
