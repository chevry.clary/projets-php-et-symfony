<?php declare(strict_types=1);
require_once('autoload.php');

$page = new WebPage("genres1");

$page->appendJsUrl("ajaxrequest.js");
$page->appendJsUrl("genres1scripts.js");

$stmt = MyPDO::getInstance()->prepare(<<<SQL
    SELECT DISTINCT name, id
    FROM genre
    ORDER BY name
SQL
);

$stmt->execute();
$genres=$stmt->fetchAll();

// Style
$page->appendContent(<<<HTML
      <select id="genres" multiple size="20">
        <option disabled>Style...</option>
HTML
);

foreach ($genres as $genre)
{
  $id=$genre['id'];
  $name=$genre['name'];
  $page->appendContent(<<<HTML
      <option value="{$id}">$name</option>
HTML
);
}

$page->appendContent(<<<HTML
      </select>
HTML
);

$page->appendContent(<<<HTML
      <select id="artistes" multiple size="20">
        <option disabled>Artiste...</option>
      </select>
      <select id="albums" multiple size="20">
        <option disabled>Album...</option>
      </select>
      <div id="div"></div>
HTML
);

echo $page->toHTML();
