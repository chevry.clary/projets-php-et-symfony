<?php declare(strict_types=1);
require_once('autoload.php');

// On demande à PHP de se reposer quelques secondes pour introduire une latence
if (isset($_REQUEST['wait'])) {
    usleep(rand(0, 20) * 100000) ;
}

$page = new WebPage;

$substring = $_GET["q"];

$stmt = MyPDO::getInstance()->prepare(<<<SQL
    SELECT id, name
    FROM artist
    WHERE lower(name) LIKE lower('%$substring%')
    ORDER BY name
SQL
);

$stmt->execute();

while (($ligne = $stmt->fetch()) !== false)
{
  echo "<p>{$ligne['name']}\n";
}
