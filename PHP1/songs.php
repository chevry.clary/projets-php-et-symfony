<?php declare(strict_types=1);
require_once('autoload.php');

// On demande à PHP de se reposer quelques secondes pour introduire une latence
if (isset($_REQUEST['wait'])) {
    usleep(rand(0, 20) * 100000) ;
}

$page = new WebPage;

$albums = $_GET["q"];

$stmt = MyPDO::getInstance()->prepare(<<<SQL
    SELECT DISTINCT LPAD(t.number,2,0) as "num", s.name as "name", DATE_FORMAT(SEC_TO_TIME(t.duration), '%i:%s') as "duration"
    FROM album al, track t, song s
    WHERE al.id = t.albumId
    AND t.songId = s.id
    AND al.id = :id
    ORDER BY t.number
SQL
);

$stmt->bindParam(':id',$albums);

$stmt->execute();

$sons=$stmt->fetchAll();

echo json_encode($sons);
