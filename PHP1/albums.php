<?php declare(strict_types=1);
require_once('autoload.php');

// On demande à PHP de se reposer quelques secondes pour introduire une latence
if (isset($_REQUEST['wait'])) {
    usleep(rand(0, 20) * 100000) ;
}

$page = new WebPage;

$artiste = $_GET["q"];

$stmt = MyPDO::getInstance()->prepare(<<<SQL
    SELECT al.id as "id", CONCAT(al.year, " - ", al.name) as "txt"
    FROM album al INNER JOIN artist ar ON al.artistId = ar.id
    WHERE al.artistId = :id
    ORDER BY al.year,al.name
SQL
);

$stmt->bindParam(':id',$artiste);

$stmt->execute();

$albums=$stmt->fetchAll();

echo json_encode($albums);
