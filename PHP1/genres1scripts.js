var ajax = null;

function viderSelect(le_select)
{
  var taille = le_select.length;
  for (var i = taille-1; i >= 1 ; i--)
  {
    le_select.remove(i);
  }
}

function ajouterOption(le_select, txt, val)
{
  var option = document.createElement("option");
  option.text = txt;
  option.value = val;
  le_select.add(option);
}

function viderNoeud(le_noeud)
{
  while (le_noeud.hasChildNodes())
  {
    le_noeud.removeChild(le_noeud.childNodes[0]);
  }
}

function charge(url, str, le_select)
{
  if (ajax != null)
    ajax.cancel();

  var ajax = new AjaxRequest(
    {
     url : url,
     method : "get",
     handleAs : "json",
     parameters : { q : str,
                    wait : null},
     onSuccess : function (result) {
      viderSelect(le_select);
      var resultat = eval(result);
      resultat.forEach(content=>ajouterOption(le_select,content["txt"],content["id"]));
      }
     }
  ) ;
}

function chargeSongs(url, str, le_select)
{
  if (ajax != null)
    ajax.cancel();

  var ajax = new AjaxRequest(
    {
     url : url,
     method : "get",
     handleAs : "json",
     parameters : { q : str,
                    wait : null},
     onSuccess : function (result) {
        result.forEach(content=>{
        var p = document.createElement("p");
        var t = document.createTextNode(content['num'] + ' - ' + content['name'] + ' - ' + content['duration']);
        p.appendChild(t);
        div.appendChild(p);
        });
      }
     }
  ) ;
}

window.onload = function ()
{
  var div = document.getElementById("div");

  document.getElementById("genres").onchange = function()
  {
      var genre = document.getElementById("genres").value;
      var artiste = document.getElementById("artistes");
      var album = document.getElementById("albums");

      viderSelect(artiste);
      viderSelect(album);
      viderNoeud(div);

      charge("artists.php",genre,artiste);

    artiste.onchange = function()
    {
      viderSelect(album);
      viderNoeud(div);
      charge("albums.php",artiste.value,album);

      album.onchange = function()
      {
        viderNoeud(div);
        var h1 = document.createElement("h1");
        var t = document.createTextNode(artiste.options[artiste.selectedIndex].text + " - " + album.options[album.selectedIndex].text);
        h1.appendChild(t);
        div.appendChild(h1);
        chargeSongs("songs.php",album.value,div);
      }
    }
    viderSelect(album);
  }
}
