<?php declare(strict_types=1);
require_once('autoload.php');

// On demande à PHP de se reposer quelques secondes pour introduire une latence
if (isset($_REQUEST['wait'])) {
    usleep(rand(0, 20) * 100000) ;
}

$page = new WebPage;

$genre = $_GET["q"];

$stmt = MyPDO::getInstance()->prepare(<<<SQL
    SELECT DISTINCT ar.id as "id", ar.name as "txt"
    FROM album al INNER JOIN artist ar ON al.artistId = ar.id
    WHERE al.genreId = :id
    ORDER BY ar.name
SQL
);

$stmt->bindParam(':id',$genre);

$stmt->execute();

$artistes=$stmt->fetchAll();

echo json_encode($artistes);
